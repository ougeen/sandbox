import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {debounceTime, map} from 'rxjs/operators';

import {GraphqlService} from './shared/graphql/graphql.service';
import {Repo, RepoNode} from './shared/types/repo';
import {RepoResponse} from './shared/types/response';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public repoName = new FormControl();
  public allRepos: Repo[] = [];

  constructor(
    private graphQlApi: GraphqlService,
  ) {
  }

  ngOnInit() {
    this.repoName
      .valueChanges
      .subscribe(key => {
      this.searchRepos(key);
    });
  }

  searchRepos(val: string): void {
    this.graphQlApi
      .getRepos(val)
      .pipe(
        debounceTime(200),
        map((response: RepoResponse) => {
          return [...response.data.search.edges];
        })
      )
      .subscribe((repos: RepoNode[]) => {
        this.allRepos = repos.map(repo => repo.node);
      });
  }
}
