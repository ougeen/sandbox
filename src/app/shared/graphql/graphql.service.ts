import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import {RepoResponse} from '../types/response';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GraphqlService {
  constructor(private apollo: Apollo) { }

  getRepos(key?: string): Observable<RepoResponse> {
    const searchKey = key ? ', ' + key : '';

    return this.apollo.query({
      query: gql`
      query {
        search(query: "is:public${searchKey}", type: REPOSITORY, first: 10) {
          repositoryCount
          edges {
            node {
              ... on Repository {
                name
                url
              }
            }
          }
        }
      }`
    });
  }
}
