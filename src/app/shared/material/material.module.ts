import {NgModule} from '@angular/core';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule, MatAutocompleteModule, MatInputModule} from '@angular/material';


const materialImports = [
  BrowserAnimationsModule,
  MatFormFieldModule,
  MatInputModule,
  MatAutocompleteModule,
];

@NgModule({
  declarations: [],
  imports: [...materialImports],
  exports: [...materialImports],
  providers: []
})
export class MaterialModule {
}
