import {RepoNode} from './repo';

export interface RepoResponse {
  data: {
    search: {
      edges: RepoNode[]
    }
  };
}
