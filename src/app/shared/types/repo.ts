export interface Repo {
  name: string;
  url: string;
}
export interface RepoNode {
  node: Repo;
}
